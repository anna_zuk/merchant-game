package Animation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class Sprite {
    private BufferedImage spriteSheet;
    private final int TILE_SIZE = 32;

    private ArrayList<BufferedImage> walkingRight;
    private ArrayList<BufferedImage> walkingLeft;
    private ArrayList<BufferedImage> walkingUp;
    private ArrayList<BufferedImage> walkingDown;
    private BufferedImage standingRight;
    private BufferedImage standingLeft;
    private BufferedImage standingUp;
    private BufferedImage standingDown;

    public ArrayList<BufferedImage> getWalkingRight() {
        return walkingRight;
    }

    public ArrayList<BufferedImage> getWalkingLeft() {
        return walkingLeft;
    }

    public ArrayList<BufferedImage> getWalkingUp() {
        return walkingUp;
    }

    public ArrayList<BufferedImage> getWalkingDown() {
        return walkingDown;
    }

    public BufferedImage getStandingRight() { return standingRight; }

    public BufferedImage getStandingLeft() { return standingLeft; }

    public BufferedImage getStandingUp() { return standingUp; }

    public BufferedImage getStandingDown() {
        return standingDown;
    }

    public Sprite(String file) {
        spriteSheet = null;

        try {
            spriteSheet = ImageIO.read(new File("resources/graphics/spritesheets/" + file + ".png"));

            walkingRight = new ArrayList<BufferedImage>(Arrays.asList(getSprite(1, 2), getSprite(0, 2), getSprite(2, 2)));
            walkingLeft = new ArrayList<BufferedImage>(Arrays.asList(getSprite(1, 1), getSprite(0, 1), getSprite(2, 1)));
            walkingDown = new ArrayList<BufferedImage>(Arrays.asList(getSprite(1, 0), getSprite(0, 0), getSprite(2, 0)));
            walkingUp = new ArrayList<BufferedImage>(Arrays.asList(getSprite(1, 3), getSprite(0, 3), getSprite(2, 3)));
            standingRight = getSprite(1, 2);
            standingLeft = getSprite(1, 1);
            standingUp = getSprite(1, 3);
            standingDown = getSprite(1, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getSprite(int xGrid, int yGrid) {
        return spriteSheet.getSubimage(xGrid * TILE_SIZE, yGrid * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }
}