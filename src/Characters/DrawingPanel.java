package Characters;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import Animation.Sprite;

import static java.awt.event.KeyEvent.*;

public class DrawingPanel extends JPanel implements KeyListener {
    private Direction direction;
    private boolean isCharacterMoving = false;
    private Character character;
    private boolean[] keys = new boolean[256];
    ArrayList<MovingCharacters> movingCharacters;

    public DrawingPanel(ArrayList<MovingCharacters> movingCharacters) {
        this.movingCharacters = movingCharacters;
        character = new Character();
        direction = Direction.RIGHT;

        Timer characterTimer = new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                character.move();
                repaint();
            }
        });
        characterTimer.start();

        Timer characterAnimationTimer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(isCharacterMoving){
                    character.loopAnimation();
                }
                repaint();
            }
        });
        characterAnimationTimer.start();

        Timer npcTimer = new Timer(450, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (MovingCharacters character : movingCharacters) {
                    character.move();
                    character.loopAnimation();
                }
                repaint();
            }
        });
        npcTimer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (MovingCharacters character : movingCharacters) {
            character.draw(g);
        }
        character.draw(g);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(32, 32);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() > 0 && e.getKeyCode() < 256) {
            keys[e.getKeyCode()] = true;
        }

        if (keys[KeyEvent.VK_D] || keys[VK_RIGHT]) {
            direction = Direction.RIGHT;
            isCharacterMoving = true;
        }

        if (keys[KeyEvent.VK_A] || keys[KeyEvent.VK_LEFT]) {
            direction = Direction.LEFT;
            isCharacterMoving = true;
        }

        if (keys[KeyEvent.VK_W] || keys[KeyEvent.VK_UP]) {
            direction = Direction.UP;
            isCharacterMoving = true;
        }

        if (keys[KeyEvent.VK_S] || keys[KeyEvent.VK_DOWN]) {
            direction = Direction.DOWN ;
            isCharacterMoving = true;
        }
    }

    public void keyReleased(KeyEvent e) {
        int[] movementKeys = {KeyEvent.VK_LEFT, KeyEvent.VK_DOWN, KeyEvent.VK_UP, VK_RIGHT, KeyEvent.VK_D, KeyEvent.VK_A, KeyEvent.VK_W, KeyEvent.VK_S};

        if (e.getKeyCode() > 0 && e.getKeyCode() < 256) {
            keys[e.getKeyCode()] = false;
        }

        for (int movementKey : movementKeys) {
            if (movementKey == e.getKeyCode())
                isCharacterMoving = false;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public class Character {
        private static final int INCREMENT = 5;
        private int x, y, animationCounter;
        private Sprite sprite;

        public Character() {
            x = 5;
            y = 30;
            animationCounter = 0;
            sprite = new Sprite("$Lanto187");
        }

        public void loopAnimation(){
            animationCounter = (animationCounter == 2) ? 0 : animationCounter + 1;
        }

        public void draw(Graphics g) {
            BufferedImage character = null;

            if(!isCharacterMoving) animationCounter = 0;
            switch (direction) {
                case RIGHT:
                    character = sprite.getWalkingRight().get(animationCounter);
                    break;
                case LEFT:
                    character = sprite.getWalkingLeft().get(animationCounter);
                    break;
                case UP:
                    character = sprite.getWalkingUp().get(animationCounter);
                    break;
                case DOWN:
                    character = sprite.getWalkingDown().get(animationCounter);
                    break;
                default:
                    character = sprite.getWalkingDown().get(animationCounter);
                    break;
            }
            g.drawImage(character, x, y, null);
        }

        public void move() {
            switch (direction) {
                case RIGHT:
                    if (x >= 5 && x < 495 && isCharacterMoving) {
                        x += INCREMENT;
                    }
                    break;
                case LEFT:
                    if (x > 5 && x <= 495 && isCharacterMoving) {
                        x -= INCREMENT;
                    }
                    break;
                case UP:
                    if (y > 30 && y <= 360 && isCharacterMoving) {
                        y -= INCREMENT;
                    }
                    break;
                case DOWN:
                    if (y >= 30 && y < 360 && isCharacterMoving) {
                        y += INCREMENT;
                    }
                    break;
            }
        }
    }
}
