package Characters;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

import Animation.Sprite;

public class NPC implements MovingCharacters {
    private static final int startX = 106;
    private static final int endX = 214;
    private static final int startY = 166;
    private static final int endY = 217;
    private static final int INCREMENT = 5;
    private int x, y;
    private Sprite sprite;
    private Direction direction;
    private int animationCounter;

    public NPC() {
        x = startX;
        y = startY;

        sprite = new Sprite("$Lanto (1)");
        direction = Direction.DOWN;
    }

    public void loopAnimation(){
        animationCounter = (animationCounter == 2) ? 0 : animationCounter + 1;
    }

    public void draw(Graphics g) {
        BufferedImage npc = null;

        switch (direction) {
            case RIGHT:
                npc = sprite.getWalkingRight().get(animationCounter);
                break;
            case LEFT:
                npc = sprite.getWalkingLeft().get(animationCounter);
                break;
            case UP:
                npc = sprite.getWalkingUp().get(animationCounter);
                break;
            case DOWN:
                npc = sprite.getWalkingDown().get(animationCounter);
                break;
        }
        g.drawImage(npc, x, y, null);
    }

    public void move() {
        Random random = new Random();
        Direction[] directions = Direction.values();
        direction = directions[random.nextInt(4)];

        boolean isEnd = false;

        switch (direction) {
            case RIGHT:
                isEnd = (x == endX);
                if (isEnd) {
                    direction = Direction.LEFT;
                    x -= INCREMENT;
                } else {
                    x += INCREMENT;
                }
                break;
            case LEFT:
                isEnd = (x == startX);
                if (isEnd) {
                    direction = Direction.RIGHT;
                    x += INCREMENT;
                } else {
                    x -= INCREMENT;
                }
                break;
            case UP:
                isEnd = (y == startY);
                if (isEnd) {
                    direction = Direction.DOWN;
                    y += INCREMENT;
                } else {
                    y -= INCREMENT;
                }
                break;
            case DOWN:
                isEnd = (y == endY);
                if (isEnd) {
                    direction = Direction.UP;
                    y -= INCREMENT;
                } else {
                    y += INCREMENT;
                }
                break;
        }
    }
}

