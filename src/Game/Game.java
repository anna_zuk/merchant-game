package Game;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

import Characters.DrawingPanel;
import Characters.MovingCharacters;
import Characters.NPC;

public class Game extends JFrame {
    private int windowHeight = 401;
    private int windowWidth = 534;

    public void run() {
        initialize();
        boolean isRunning = true;

        while (isRunning) {
            long time = System.currentTimeMillis();

            time = (1000 / 60) - (System.currentTimeMillis() - time);

            if (time > 0) {
                try {
                    Thread.sleep(time);
                } catch (Exception e) {}
            }
        }
        setVisible(false);
    }

    public void initialize() {
        setTitle("Merchant Game");
        setSize(windowWidth, windowHeight);

        ImageIcon icon = new ImageIcon("resources/graphics/town/barrel.png");
        setIconImage(icon.getImage());

//        ArrayList<ArrayList<GridSquare>> gameSurface = new ArrayList<ArrayList<GridSquare>>(dimx, dimy);

        JPanel staticPanel = new JPanel(){
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                BufferedImage bg = null;
                try{
                    bg = ImageIO.read(new File("resources/graphics/town/scene.png"));
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                g.drawImage(bg, 0,0, windowWidth, windowHeight, null);
            }
        };

        staticPanel.setLayout(new BorderLayout());

        ArrayList<MovingCharacters> movingCharacters = new ArrayList();
        movingCharacters.add(new NPC());

        DrawingPanel drawingPanel = new DrawingPanel(movingCharacters);
        drawingPanel.addKeyListener(drawingPanel);
        drawingPanel.setFocusable(true);
        drawingPanel.setOpaque(false);
        staticPanel.add(drawingPanel);

        add(staticPanel);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}
