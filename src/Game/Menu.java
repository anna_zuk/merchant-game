package Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu {
    public static void run() {
        JFrame jFrame = new JFrame();
        jFrame.setBounds(12, 12, 1083, 760);
        jFrame.setTitle("Merchant Game");

        JLayeredPane layeredPane = new JLayeredPane();
        layeredPane.setPreferredSize(new Dimension(1083, 760));

        ImageIcon icon = new ImageIcon("resources/graphics/background.png");
        JLabel background = new JLabel(icon);
        background.setOpaque(false);
        background.setBounds(0, 0, icon.getIconWidth(), icon.getIconHeight());

        JButton button = new JButton();
        button.setIcon(new ImageIcon("resources/graphics/gui/play-btn.png"));
        button.setBounds(icon.getIconWidth() / 2, icon.getIconHeight() / 2, 86, 88);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game game = new Game();
                game.run();
            }
        });

        layeredPane.add(button, new Integer(2));
        layeredPane.add(background, new Integer(1));

        jFrame.setResizable(false);
        jFrame.setContentPane(layeredPane);
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
